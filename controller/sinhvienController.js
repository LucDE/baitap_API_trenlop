const ten_sv = document.querySelector("#txtTenSV");
const email_sv = document.querySelector("#txtEmail");
const hinhanh = document.querySelector("#txtHinhAnh");
const maSV = document.querySelector("#txtMaSV");

//lấy input từ người nhập
function layThongTinTuForm() {
  var ma_sv = maSV.value.trim();
  var tenSV = ten_sv.value.trim();
  var emailSV = email_sv.value.trim();
  var hinhAnhSV = hinhanh.value.trim();
  var sv = new SinhVien(ma_sv, tenSV, emailSV, hinhAnhSV);
  return sv;
}
function showInfoIntoForm(sv) {
  maSV.value = sv.ma;
  ten_sv.value = sv.ten;
  email_sv.value = sv.email;
  hinhanh.value = sv.hinhAnh;
}
