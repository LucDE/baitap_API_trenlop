var dssv = [];
var index_update;
const BASE_URL = "https://62f8b7483eab3503d1da151c.mockapi.io";
//axios bất đồng bộ
renderDssvService();
function xoaSV(id) {
  turnOnLoading();
  console.log("id: ", id);
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "delete",
  })
    .then(function () {
      turnOffLoading();
      renderDssvService();
    })
    .catch(function (err) {
      turnOffLoading();
      console.log("err: ", err);
    });
}
function suaSinhVien(id) {
  xemSV(id);
}
function themSV() {
  turnOnLoading();
  var sv = layThongTinTuForm();
  axios({
    url: `${BASE_URL}/sv`,
    method: "post",
    data: sv,
  })
    .then(function () {
      turnOffLoading();
      renderDssvService();
    })
    .catch(function (err) {
      turnOffLoading();
      console.log("err: ", err);
    });
}
function capNhatSV() {
  turnOnLoading();
  var sv = layThongTinTuForm();
  axios({
    url: `${BASE_URL}/sv/${sv.ma}`,
    method: "put",
    data: sv,
  })
    .then(function () {
      turnOffLoading();
      renderDssvService();
    })
    .catch(function (err) {
      turnOffLoading();
      console.log("err: ", err);
    });
}
function xemSV(id) {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "GET",
  })
    .then(function (response) {
      turnOffLoading();
      var sv_can_lay = response.data;
      showInfoIntoForm(sv_can_lay);
    })
    .catch(function (error) {
      turnOffLoading();
      console.log("error: ", error);
    });
  return (index_update = id);
}
function renderDssvService() {
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    //response object
    .then(function (res) {
      turnOffLoading();
      // console.log("res: ", res);
      dssv = res.data;
      console.log("dssv: ", dssv);
      renderTable(dssv);
    })
    //error object
    .catch(function (err) {
      turnOffLoading();
      console.log("err: ", err);
    });
}
var renderTable = function (list) {
  var contentHTML = "";
  list.forEach(function (item) {
    var trContent = ` <tr> <td>${item.ma}</td>
<td>${item.ten}</td>
<td>${item.email}</td>
<td><img src=${item.hinhAnh} style="width:40px" alt=""/></td>     <td><button class="btn btn-danger" onclick = "xoaSV('${item.ma}')">Xóa</button>
    <button class="btn btn-success" onclick = "suaSinhVien('${item.ma}')">Sửa</button></td>
</tr>`;
    contentHTML += trContent;
  });
  document.querySelector("#tbodySinhVien").innerHTML = contentHTML;
};
var turnOnLoading = function () {
  document.querySelector("#loading").style.display = "flex";
};
var turnOffLoading = function () {
  document.querySelector("#loading").style.display = "none";
};
